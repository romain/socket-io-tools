<?php
namespace Romain\SocketIO;

use \ElephantIO\Client;
use \ElephantIO\Engine\SocketIO\Version1X;

/**
 * Mini client Socket IO Elephant
 */
Class ElephantClient {
	use \Romain\Tools\Pattern\SingletonTrait;

	/**
	 *
	 * @var ElephantClient
	 */
	protected static $_instance = null;

	/**
	 *
	 * @var string
	 */
	protected $_host = null;
	
	/**
	 *
	 * @var Client
	 */
	protected $_elephant = null;

	/**
	 *
	 * @param string $host
	 */
	public function __construct($host = null) {
		if(empty($host)) {
			$host = 'http://localhost:3000';
		}
		$this->_host = $host;
		$this->init();
	}

	/**
	 *
	 */
	public function __destruct() {
		$this->close();
	}

	/**
	 *
	 */
	public function init() {
		if(!($this->_elephant instanceof ElephantClient)) {
			$this->_elephant = new Client(new Version1X($this->_host));
			$this->_elephant->initialize();
		}
	}

	/**
	 *
	 * @param string $name
	 * @param mixed $data
	 */
	public function emit($name, $data) {
		try {
			$this->_elephant->emit($name, $data);
		} catch (\Exception $ex) {}
	}

	/**
	 *
	 * @return boolean
	 */
	public function close() {
		if(!($this->_elephant instanceof ElephantClient)) {
			return false;
		}
		
		try {
			$this->_elephant->close();
		} catch (\Exception $ex) {}
	}
	
}